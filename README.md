# LaTeX template for the Journal of Industrial Ecology

The Journal of Industrial Ecology (JIE) does currently not provide a template for submissions using LaTeX. 
This repository contains a LaTeX file that mimics the author guidelines as closely as possible.

# TODO

*This project is under development.*

1. [ ] Upload template


## Usage

Download folder manuscript & enjoy 
or `git clone https://gitlab.com/ISIE/JIE-latex-template.git`

## Formatting

The JIE follows the [Chicago Manual of Style (CMS), 15th edition](http://www.chicagomanualofstyle.org/home.html). 
The following sections document some formatting peculiarities for LaTeX.

### Layout

The author guidelines require 12pt font size, double spacing, and a 12 inch indent for first lines. 
This can be achieved with the following:

```latex
\documentclass[12pt]{article}
\usepackage[doublespacing]{setspace}
\usepackage{indentfirst} % also indent the first paragraph of a section http://tex.stackexchange.com/a/39228
\parindent=0.5in % paragraph first line indent
```

### Line numbers

The package `\usepackage{lineno}` enables line numbering. 
Now it is possible to wrap the text between `\begin{linenumbers}` and `\end{linenumbers}` 
or switch it on for the whole article with `\linenumbers` after `\begin{document}`.

### Section headings

JIE uses a particular formatting for section headings. They should be preceded by `<heading level 1>`.
The following code can be used to 'hack' the LaTeX `\section` command, based on [this SE answer]( http://tex.stackexchange.com/a/68487/67042).

```latex
\renewcommand\thesection{\textless heading level 1\textgreater}
\renewcommand\thesubsection{\textless heading level 2\textgreater}
\renewcommand\thesubsubsection{\textless heading level 3\textgreater}
```

Referencing within the document is not possible, because no numbering exists. 
Therefore, one may want to use `\usepackage{nameref}` and e.g. `\nameref{sec:Introduction}`, which will show section titles instead of numbers.

### Bibliography 

The [`biblatex-chicago` package](https://www.ctan.org/pkg/biblatex-chicago) comes very close to the Jouenal's requirements. 
However, the following formatting settings are helpful.

```latex
\usepackage[
	authordate,
	backend=biber,
	maxcitenames=2,
	uniquelist=false
	]{biblatex-chicago}
```

## References

- [Journal of Industrial Ecology](http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1530-9290)
- [JIE author guidelines](http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291530-9290/homepage/ForAuthors.html)
- [JIE Style Guide for Authors (PDF)](http://www.yale.edu/jie/JIEstyle4authors.pdf)
- [Chicago Manual of Style (CMS), 15th edition](http://www.chicagomanualofstyle.org/home.html)
